\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{What is microlensing?}{3}{0}{1}
\beamer@sectionintoc {2}{Parameter Extraction}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{How do we navigate our parameter space?}{9}{0}{2}
\beamer@subsectionintoc {2}{2}{What problems did we face?}{11}{0}{2}
\beamer@subsectionintoc {2}{3}{How did we solve these problems?}{12}{0}{2}
\beamer@subsectionintoc {2}{4}{Okay, so how well did we do?}{13}{0}{2}
\beamer@sectionintoc {3}{Future Work}{14}{0}{3}
\beamer@subsectionintoc {3}{1}{What about future astrophysics?}{15}{0}{3}
\beamer@sectionintoc {4}{Backup Slides}{17}{1}{4}
