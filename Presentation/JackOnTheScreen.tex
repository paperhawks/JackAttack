\documentclass[xcolor=dvipsnames,mathserif]{beamer} 	%signifies the class
\usepackage{amsmath}						%for math shit
\usepackage{graphicx}   					%allows for pictures and shit
\usepackage{caption}    					%captions for graphs and figures
\usepackage{verbatim}   					%allows for commenting
\usepackage{amsthm} 						%for proofs
\hypersetup{colorlinks=true,linkcolor=blue} %make your links blue
\usepackage{fancyvrb}
\usepackage{tikz}	
\usetikzlibrary{automata,positioning}
%\usepackage{movie15}						%for making figures
%\usepackage{xmpmulti}
\usepackage{hyperref}						%for links
\usepackage{custom}							%Feickert's Custom Style file
\usetheme{Singapore}							%the Antibes themes 
\usecolortheme[named=Blue]{structure}		%the Blue color theme, please
\setbeamertemplate{footline}[slide number]
\setbeamertemplate{blocks}[rounded][shadow=true]
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\makeatletter
\newcommand{\AlignFootnote}[1]{%
  \ifmeasuring@
    \chardef\@tempfn=\value{footnote}%
    \footnotemark
    \setcounter{footnote}{\@tempfn}%
  \else
    \iffirstchoice@
      \footnote{#1}%
    \fi
  \fi}
\makeatother
\newcommand{\backupbegin}{
   \newcounter{finalframe}
   \setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
   \setcounter{framenumber}{\value{finalframe}}
}




\title[Microlensing Fitting]{\textbf{Extracting the Parameters from a Simple Microlensing Model}}
\author{Conan Huang}
\date{December 14, 2016}
\institute[Rutgers]{Rutgers, the State University of New Jersey}

\begin{document}


\AtBeginSection[]
{
	\begin{frame}
	\frametitle{Outline}
	\tableofcontents[currentsection]
	\end{frame}
}

\begin{frame}
\titlepage
\end{frame}

\section{Introduction}
\subsection{What is microlensing?}
%Microlensing is the increase of a background (source) star's flux from gravitational distortions in the foreground.
\begin{frame}
\frametitle{So what is microlensing?}
A microlensing event occurs when a massive foreground object bends a background source object's light increasing the amount of light being received. 
\begin{figure}[H]
\centering
\includegraphics[scale=.4]{microlensing_cartoon.png}
\caption{More light comes from a star when a massive foreground object passes by. [Encyclop\ae dia Britannica, Inc.]}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{What can microlensing do?}
\begin{itemize}
\item
Microlensing can be used to \textbf{observe dim objects} in the sky.
\item
Microlensing can also be useful for \textbf{finding interesting astrophysical objects} such as black holes.
\item
This has been used to \textbf{detect extrasolar planets.} (See OGLE-2003-BLG-235/MOA 2003-BLG-53.)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{This is what the data looks like.}
\begin{figure}[H]
\centering
\includegraphics[scale=.45]{rawr_data.png}
\caption{Source: \url{http://wwwmacho.anu.edu.au/Data/LMC/Lensing/}}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{So what is the physics?}
These three equations.

\begin{itemize}
\item
$u(t) = \sqrt{\left( u_\mathrm{min}^2 + \left(\frac{t-t_0}{t_E}\right)^2 \right)}$ - Distance on the plane between the source object and foreground object
\begin{itemize}
\item
$u_\mathrm{min}$ - Our $y$ distance on the plane, like the impact parameter.
\item
$t_0$ - Center of the microlensing event in time.
\item
$t_E$ - Einstein crossing time. 
\end{itemize}
\item
$A(u) = \dfrac{u^2+2}{u\sqrt{u^2+4}}$ - Amplification factor
\item
$f_\mathrm{obs} = A(1-\eta)f_0 + \eta f_0$ - Observed flux
\begin{itemize}
\item
$\eta$ - Blending parameter
\item
$f_0$ - Base flux.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{This is what $f_\mathrm{obs}$ looks like.}
\begin{figure}[H]
\centering
\includegraphics[scale=.5]{f_obs_eta.png}
\caption{Plot showing how $f_\mathrm{obs}$ looks for different values of $\eta$.}
\end{figure}
\end{frame}

\section{Parameter Extraction}

\subsection{How do we navigate our parameter space?}
%We step through using chi squared and the ratio of the likelihood functions.
\begin{frame}
\frametitle{How do we find our parameters?}
We choose to use Metropolis-Hastings to find the parameter vector because 
\begin{enumerate}
\item
There are 5 free parameters.
\item
We want to do more than just interpolate, we want the values.
\end{enumerate}

We do this by choosing our goodness of fit as 

\begin{equation}
\notag
\chi^2 = \sum_{i\in I } \frac{\left(f_i - f(t_i|\vec{\theta})\right)^2}{\sigma_i^2}.
\end{equation}
\end{frame}

\begin{frame}
\frametitle{But goodness of fit isn't all.}
We also need to choose how to step. The Gaussian likelihood 

\begin{equation}
\notag
\mathcal{L}(\vec{\theta}) = \exp\left(-\frac{\chi^2}{2}\right).
\end{equation}

Acceptance criteria:
%\min\left(1,\frac{p(\vec{y})W^\mathrm{prop}_{\vec{y}\vec{x}}}{p(\vec{x})W_{\vec{x}\vec{y}}^\mathrm{prop}}\right)=
\begin{equation}
\notag
A = \min\left(1,\frac{\mathcal{L}(\vec{\theta}_\mathrm{new})}{\mathcal{L}(\vec{\theta}_\mathrm{old})}\right) = \min\left(1,\exp\left(\frac{-\chi^2_\mathrm{new}+\chi^2_\mathrm{old}}{2}\right)\right).
\end{equation}

Compare $A$ with $r\sim \mathrm{Uniform}(0,1)$.
\end{frame}

\subsection{What problems did we face?}
%1.) Outlier problems. 2.) Problems with few number of data points during the event. 3.) Abundence of baseline points.


\begin{frame}
\frametitle{What are the difficulties?}
\begin{figure}[H]
\centering
\includegraphics[scale=.3]{rawr_data.png}
\end{figure}
\begin{enumerate}
\item
Outliers, they kind of ``look" like microlensing events.
\item
There are very few points in the microlensing event.
\item
There are many points in the baseline. 
\end{enumerate}
\end{frame}

\subsection{How did we solve these problems?}
%1.) LOWESS 2.) Choosing a good starting point. 3.) Tried clipping the data. 
\begin{frame}
\frametitle{Dealing with outliers using smoothing.}
We dealt with outliers by using locally weighted scatterplot smoothing (LOWESS). This uses a weight function 

\begin{equation}
\notag
w(x) = (1-|x|^3)^3 \mathbf{I}[|x|<1].
\end{equation}

The result is this

\begin{figure}[H]
\centering
\includegraphics[scale=.3]{rawr_smoothed.png}
\end{figure}
\end{frame}

\subsection{Okay, so how well did we do?}
%Okay, we did pretty well. Show pretty pictures.
\begin{frame}
\frametitle{The parameters and the goodness of fit is as follows.}
After 10,000 MC steps...
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{itemize}
\item
$\eta$: 0.168376224839
\item
$f_0$: 95.6743703896
\item
$t_0$: 56.254244446
\item
$t_E$: 20.1214037608 
\item
$u_\mathrm{min}$: 0.156807528866
\item
$\chi^2_\mathrm{reduced}$: 2.15737815231
\item
$\chi^2_\mathrm{event}$: 8.46286161751
\item
$\chi^2_\mathrm{base}$: 0.7399467344
\end{itemize}
\end{column}
\begin{column}{0.5\textwidth}  %%<--- here
    \begin{center}
     \includegraphics[scale=.3]{r_fit_best.png}
     \end{center}
\end{column}
\end{columns}
\end{frame}


\section{Future Work}



\subsection{What about future astrophysics?}
\begin{frame}
\frametitle{What else can we do other than fitting?}
\begin{itemize}
\item
Test asymmetries in the data.
\item
Compare with different filters. 
\item
Ask Jack.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Science.}
\begin{center}
\includegraphics[scale=.5]{futurama.png}\\
Source: Futurama S6E26
\end{center}
\end{frame}


%%%%%%APPENDIX GOES HERE%%%%%%%%%%


\appendix
\backupbegin
\section{Backup Slides}
%Could try a different goodness of fit.
\begin{frame}
\frametitle{Can we do better?}
\begin{itemize}
\item
We could try to use a different goodness of fit. (KS, Anderson-Darling,...)
\item
Clipping the data sacrifices baseline guess. But, we could guess the baseline independently. 
\item
Do entirely a local regression and interpolate on the points before fitting.
\end{itemize}
\end{frame}

\backupend
\end{document}





















































