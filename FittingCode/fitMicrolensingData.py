#standard imports
import numpy as np 
import scipy as sp 
import matplotlib.pyplot as plt
import numpy.random as rand
from copy import *

#file creation imports
import sys
import os.path

#website imports 
from lxml import html
import requests

#MC/curve fitting imports
from scipy.optimize import curve_fit
import pymc
import statsmodels.api as sm

#----------FUNCTIONS FOR OBTAINING/EXTRACTING DATA (START)----------
def extract_cols(line, n_filters):
    """
    Looks at the line and extracts the data that we need. 
    e.g. 
    -170.6981    -99.000   -99.000     -4.613     0.114       416     0
    would return the first 5 values.
    """
    row = []
    data_from_line =  line.lstrip().rstrip().split()
    row.append(float(data_from_line[0])) #store days first

    #store data and error for all filters
    for i in range(n_filters):
        row.append(float(data_from_line[2*i+1])) #data
        row.append(float(data_from_line[2*i+2]))   #data error

    return row

def parse(text, delimiter, n_filters):
    """
    Looks into the text and extracts the raw data ignoring lines that beginning
    with a certain delimiter. Assumes the data format below.

    For example the text below would return 
    #--------------------------------------------------------------------
    # HJD-2.449e+06    r       err_r        b       err_b     obsid  tran
    #    (days)      (mag)     (mag)      (mag)     (mag)
    #--------------------------------------------------------------------
      -170.6981    -99.000   -99.000     -4.613     0.114       416     0
      -169.6825     -4.693     0.105     -4.335     0.102       449     0

    -------> returns 
    -170.6981    -99.000   -99.000     -4.613     0.114       416     0
    -169.6825     -4.693     0.105     -4.335     0.102       449     0
    """
    data_list = [[] for i in range(2*n_filters + 1)]
    for line in text:

        #ignore comments in the beginning
        if line[0] != '#':
            data_line = extract_cols(line, n_filters)
            data_list[0].append(data_line[0]) #store day

            #store rest of filters
            for i in range(n_filters):
                data_list[2*i+1].append(data_line[2*i+1])
                data_list[2*i+2].append(data_line[2*i+2])
    
    return data_list

def remove_empty(x,y,yerr):
    """
    There are lines in which we have -99.000, these need to be removed. 
    This function removes these from the data.
    # Calibration: instrumental
    #--------------------------------------------------------------------
    # HJD-2.449e+06    r       err_r        b       err_b     obsid  tran
    #    (days)      (mag)     (mag)      (mag)     (mag)
    #--------------------------------------------------------------------
      -170.6981    -99.000   -99.000     -4.613     0.114       416     0
    Assumes x,y,yerr are all the same dimension.
    """
    x_ret = []
    y_ret = []
    yerr_ret = []
    for i, val in enumerate(x):
        if y[i] != -99 and yerr[i] != -99:
            x_ret.append(x[i])
            y_ret.append(y[i])
            yerr_ret.append(yerr[i])

    return np.array(x_ret), np.array(y_ret), np.array(yerr_ret)

def convert_from_mag(data_array, error_array):
    """
    Converts the data from magnitude to some arbitrary 
    units of luminosity.
    """

    return np.array([10**(-i/2.5) for i in data_array]), np.array([10**(-data_array[i]/2.5)*np.log(10)/2.5*error_array[i] for i in range(len(error_array))])

def get_data_from_url(data_url, n_filters, fname):
    """
    1.) Gets the data from the given url.
    2.) Saves the data into a flat file.
    3.) Reads the data by parsing the file.
    4.) Returns the data in arrays.
    """

    #check first if file exists before calling url
    if not os.path.isfile(fname):
        print "Obtaining data from website:", data_url
        page = requests.get(data_url)
        pageText = page.text

        #convert data to textfile for easy parsing
        text_file = open(fname, "w")
        text_file.write(pageText)
        text_file.close()

    #read data
    text_file = open(fname, "r")
    flat_data = parse(text_file, '#', n_filters)
    text_file.close()

    x_list = [[] for i in range(n_filters)]
    y_list = [[] for i in range(n_filters)]
    yerr_list = [[] for i in range(n_filters)]
    #return data for each filter
    for i in range(n_filters):
        x, y, yerr = remove_empty(flat_data[0], flat_data[2*i+1], flat_data[2*i+2])
        x_list[i] = x
        y_list[i] = y
        yerr_list[i] = yerr

    return x_list, y_list, yerr_list

def smooth_data(x,y):
    """
    Smooths data using LOWESS. Takes the data array and returns a smoothed data 
    using linear regression with a tri-cubic weighting function.
    """

    lowess = sm.nonparametric.lowess
    smoothed_data = lowess(y,x,frac=0.015)

    return smoothed_data[:,0], smoothed_data[:,1]

#----------FUNCTIONS FOR OBTAINING/EXTRACTING DATA (END)----------

#----------PHYSICS FUNCTIONS (START)----------
def u(t, t_0, t_E, u_min):
            """
            The function for the distance of the foreground image's projection to the center.
            Computes the function 

            u(t) = ( u_min^2 + (t-t_0/t_E)^2 )^(1/2)
            """
            return np.sqrt(u_min**2 + ((t-t_0)/t_E)**2)

def A(t, t_0, t_E, u_min):
        """ 
        Function computes the amplification factor.

        A(u) = (u^2+2)/(u*(u^2+4)^(1/2))
        """
        
        u_tmp = u(t, t_0, t_E, u_min)
        return (u_tmp**2+2.)/(np.sqrt(u_tmp**4+4.*u_tmp**2))

def f_obs(t, eta, f_0, t_0, t_E, u_min):
    """
    Calculates the observed luminosity.
    F_obs = (1-eta)*A*f_0 + eta*f_0
    """
    A_tmp = A(t, t_0, t_E, u_min)
    return (1 - eta)*A_tmp*f_0 + eta*f_0
#----------PHYSICS FUNCTIONS (END)----------

#----------MONTE CARLO FUNCTIONS (START)----------
def calc_chi_squared(obs_arr, exp_array, error_array, err_type = 'G'):
    """
    Calculates the chi-squared value given the parameters
    as input. 
    Sum[(o-e)^2/e] - Poisson error assumed
    Sum[(o-mu)^2/e] - Gaussian error assumed
    """
    if len(obs_arr) != len(exp_array):
        print("Array lengths are not the same to calculate chi squared.")
        sys.exit(1)

    chi_sq = 0
    if err_type == 'P':
        for i in range(len(obs_arr)):
            chi_sq += ((obs_arr[i] - exp_array[i] + 0.)**2)/exp_array[i]
    if err_type == 'G':
        # obs_mean = obs_arr.mean()
        for i in range(len(obs_arr)):
            chi_sq += ((obs_arr[i] - exp_array[i] + 0.)**2)/(error_array[i]**2)

    return chi_sq
  
def calc_step_size():
    """
    Calculates the step size given the partial derivative in each
    direction. 
    """
    return 0.01

def find_baseline(y):
    """
    Looks at only the window of the value and returns the average of that.
    INPUT: 
    ------
    1.) Array of values.

    OUTPUT:
    -------
    1.) Average in a 10% window. 
    """
    y_10pct = []
    mean = y.mean()
    for i in y:
        if np.abs(i-mean)< 0.1*mean:
            y_10pct.append(i)

    return np.array(y_10pct).mean()

def Find_Turning_Points(y):
    """
    Assumes that the function has a point in which there is a peak and finds 
    points in which the increase begins.

    Assumes symmetry about the maximum value.
    """
    max_index = y.argmax()
    mean = y.mean()

    #find the point that is within 50% of mean (approx FWHM) expanding from the max value
    for i in range(len(y)):
        if (y[max_index-i] - mean) < 0.5*mean:
            return max_index-i, max_index+i

def MC_init(x,y):
    """
    Returns the roughest guess for the parameters in the order of the 
    array:
    "eta, f_0, t_0, t_E, u_min"
    Uses physical intuition to determine certain parameters such as 
    f_0 - average of the LOWESS data
    t_0 - max value's x
    t_E - approx width
    """
    #return [.5, 100, 20, 20, .1] #initial guess
    #return [0.3658530833821976, 95.720814540317349, 56.328641387370652, 20.187339416292016, 0.13928094551026993] #optimal guess
    #find baseline guess
    turning_index_1, turning_index_2 = Find_Turning_Points(y)
    width = x[turning_index_2]-x[turning_index_1]
    baseline = find_baseline(y)
    print "baseline:", baseline
    print "width:", x[turning_index_2]-x[turning_index_1]

    return [0.1, baseline, x[y.argmax()], width, 0.1]

def get_step_vector(step_size, params_list, x, y):
    """
    Returns a vector that steps through the parameter space.
    """
    #create new point in parameter space
    step_tolerance = 0.000001
    step_factor = 10
    step_vec = []
    new_params = []
    for i in params_list:
        new_params.append(i)

    #loop through each direction
    for i in range(len(params_list)):
        proposal_step = rand.uniform(0,step_size)

        #calculate finite difference
        new_params[i] = params_list[i] + proposal_step #update to new step in chosen direction
        delta_f = f_obs(x[y.argmax()], new_params[0], new_params[1], new_params[2], new_params[3], new_params[4]) - f_obs(x[y.argmax()], params_list[0], params_list[1], params_list[2], params_list[3], params_list[4])

        for i in range(1000):

            #increase step size by delta_f 
            if delta_f > step_tolerance:
                proposal_step = proposal_step * step_factor
            else: 
                break

            new_params[i] = params_list[i] + proposal_step #update to new step in chosen direction
            delta_f = f_obs(x[y.argmax()], new_params[0], new_params[1], new_params[2], new_params[3], new_params[4]) - f_obs(x[y.argmax()], params_list[0], params_list[1], params_list[2], params_list[3], params_list[4])


        step_vec.append(proposal_step)

    return step_vec

def Check_Min(old_chi_sq, new_chi_sq, old_params, new_params):
    """
    Checks the two chi-squred values and sees which one is smaller.
    Returns the smaller chi-squared and the list of parameters associated with
    the smaller one.
    """
    #case when the new chi squared is better
    if new_chi_sq < old_chi_sq:
        return new_chi_sq, new_params
    else:
        return old_chi_sq, old_params

def MetroHastings_Sampling(x, y, yerr, MC_steps):
    """
    Calculates the acceptance criteria in accordance to Metropolis-Hastings
    algorithm. 

    INPUT:
    ------
    1.) x - raw data's x coordinate
    2.) y - raw data's y coordinate
    3.) yerr - error in the y coordinate's measurement
    4.) MC_steps - number of Monte Carlo steps to be taken

    OUTPUT: 
    -------
    1.) New vector in parameter space after the MC run. 
    2.) History of the params list as a function of MC steps.
    """

    #initialize parameter space starting point
    params_hist = []
    params_list = MC_init(x,y)
    chi_sq = calc_chi_squared(y, f_obs(x,params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]), yerr, 'G')
    min_chi_sq = np.inf 
    min_params = [] 

    #perform MC steps
    for i in range(MC_steps):
        #calculate new step size
        step_size = calc_step_size()
        step_vec = []

        #create the step in parameter space
        step_vec = get_step_vector(step_size, params_list, x, y)

        #DEBUG PRINT BELOW
        # if i < 5:
        #     print "Step vector:", step_vec

        for j in range(len(params_list)):
            params_list[j] += step_vec[j]

        #DEBUG PRINT BELOW
        # if i < 5:
        #     print "Params:", params_list

        new_chi_sq = calc_chi_squared(y, f_obs(x,params_list[0], params_list[1], params_list[2], params_list[3], params_list[4]), yerr, 'G')

        #check if worse, if so accept with MH-criteria
        if new_chi_sq > chi_sq:

            #create acceptence based on ratios of chi squared
            chi_ratio = np.exp(-(new_chi_sq-chi_sq)/2)
            acceptence = min(1.,chi_ratio)
     
            if rand.random() > acceptence:

                #don't accept, move back 
                for j in range(len(params_list)):
                    params_list[j] -= step_vec[j]
            else: 
                chi_sq = new_chi_sq
                params_hist.append(copy(params_list))
                min_chi_sq, min_params = Check_Min(min_chi_sq, new_chi_sq, min_params, params_list) #returns minimum values
        else:
            chi_sq = new_chi_sq
            params_hist.append(copy(params_list))
            min_chi_sq, min_params = Check_Min(min_chi_sq, new_chi_sq, min_params, params_list) #returns minimum values

    print "Final chi-squared:", chi_sq
    print "Final reduced chi-squared", chi_sq / (len(y) - len(params_list))
    return min_params, params_hist

def MC_Naive_Sampling(x, y, yerr, MC_steps):
    #initialize the parameter space starting point
    params_list = MC_init(x,y)
    chi_sq = calc_chi_squared(y, f_obs(x,params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]), yerr, 'G')

    #perform MC steps
    for i in range(MC_steps):
        #calculate new step size
        step_size = calc_step_size()
        step_vec = []

        for i in range(len(params_list)):
            step_vec = get_step_vector(step_size, params_list, x, y)
            params_list[i] += step_vec[i]

        new_chi_sq = calc_chi_squared(y, f_obs(x,params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]), yerr, 'G')

        #check if chi-squared is worse 
        if new_chi_sq > chi_sq:
            for i in range(len(params_list)):
                params_list[i] -= step_vec[i]
        else: 
            chi_sq = new_chi_sq

    return params_list

#----------MONTE CARLO FUNCTIONS (END)----------

#----------PLOTTING/PRINTING FUNCTIONS (START)----------
def plot_luminosity_data(x,y,yerr):
    """
    Plots the data with error bars for one filter.
    """
    #plot figure
    plt.figure()
    plt.errorbar(x, y, yerr=yerr, fmt='o')
    plt.suptitle('Luminosity Data Fit', fontsize=20)
    plt.xlabel('Days', fontsize=18)
    plt.ylabel('r (flux)', fontsize=16)
    plt.show()

def Plot_Data_and_Fit(x,y,yerr, x_fit, y_fit):
    """
    Plots the data with the fit with error bars for one filter.
    """
    plt.figure()
    plt.errorbar(x, y, yerr=yerr, fmt='o')
    plt.errorbar(x_fit, y_fit, yerr=0)
    plt.suptitle('Luminosity Data Fit', fontsize=20)
    plt.xlabel('Days', fontsize=18)
    plt.ylabel('r (flux)', fontsize=16)
    plt.show()

def Plot_History(data, index):
    """
    Plots the history vs MC Steps.
    """
    y = [i[index] for i in data]
    plt.figure()
    plt.errorbar(range(len(data)), y, yerr=0, fmt='o')
    plt.suptitle('Paramter History', fontsize=20)
    plt.xlabel('MC Steps', fontsize=18)
    plt.ylabel('Parameter' + str(index), fontsize=16)
    plt.show()

def Print_Parameters(params_list):
    """
    Print parameters for each value.
    TODO: Generalize to dictionary.
    """
    print "\n-----PARAMETERS-----"
    print "eta:", params_list[0]
    print "f_0:", params_list[1]
    print "t_0:", params_list[2]
    print "t_E:", params_list[3]
    print "u_min", params_list[4]
    print "params list:", params_list
    print "-----PARAMETERS-----\n"

def Print_Debug(y_mean, y_std, y_smooth_mean, y_smooth_std, params_hist):
    """
    Prints debugging parameters relavent to me. (Conan)
    """
    print "\n-----DEBUG INFO-----"
    print "y-mean:", y_mean
    print "y-std:", y_std
    print "smoothed y-mean:", y_smooth_mean
    print "smoothed y-std:", y_smooth_std
    print "Params history:", params_hist #for debugging
    print "-----DEBUG INFO-----\n"

#----------PLOTTING/PRINTING FUNCTIONS (END)----------

def main():
    #Monte Carlo parameters
    MC_steps = 10000

    #obtain data from website 
    #data from http://wwwmacho.anu.edu.au/Data/fts.html
    n_filters = 2
    fname = '79.5628.1547.publc'
    data_url = 'http://wwwmacho.anu.edu.au/Data/LMC/Lensing/'+fname
    x_list,y_list,yerr_list = get_data_from_url(data_url, n_filters, fname)

    #perform analysis for each filter
    for i in range(n_filters):
        x,y,yerr = x_list[i], y_list[i], yerr_list[i]
        x_plot = np.linspace(x[0], x[len(x)-1], 3000)
        y , yerr = convert_from_mag(y, yerr)

        #smooth the data and remove outliers using LOWESS
        x_smooth, y_smooth = smooth_data(x,y)

        #sample parameter space for data 
        # params_list = MC_Naive_Sampling(x, y, yerr, MC_steps)
        params_list, params_hist = MetroHastings_Sampling(x_smooth, y_smooth, yerr, MC_steps)
        # params_list = [0.16837622483945722, 95.674370389645674, 56.254244445995106, 20.121403760846661, 0.15680752886558402]

        #Calculating baseline vs. event chi-squared separetely 
        # lower_cut = 50
        # upper_cut = 150
        # print "Chi-sq event:",(calc_chi_squared(y_smooth[lower_cut:upper_cut], f_obs(x_smooth[lower_cut:upper_cut],params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]), yerr[lower_cut:upper_cut]))/(upper_cut-lower_cut-len(params_list) )
        # base_y = np.array(list(y_smooth[:lower_cut]) + list(y_smooth[upper_cut:]))
        # base_x = np.array(list(x_smooth[:lower_cut]) + list(x_smooth[upper_cut:]))
        # base_err = np.array(list(yerr[:lower_cut]) + list(yerr[upper_cut:]))
        # print "Chi-sq baseline:",(calc_chi_squared(base_y, f_obs(base_x,params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]), base_err))/(len(x_smooth) - (upper_cut-lower_cut)-len(params_list) )
        

        Plot_Data_and_Fit(x_smooth,y_smooth,yerr, x_plot, f_obs(x_plot,params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]))
        
        # Plot_History(params_hist, 4)
        # plot_luminosity_data(x,f_obs(x,params_list[0], params_list[1],params_list[2], params_list[3], params_list[4]), yerr=0)
        # plot_luminosity_data(x_smooth,y_smooth,yerr)
        
        # Print_Debug(y.mean(), y.std(), y_smooth.mean(), y_smooth.std(), [i[0] for i in params_hist[:10]])
        Print_Parameters(params_list)


if __name__ == '__main__':
    main()